{
  description = "Follow video creators";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.nixpkgs-gnome.url = "github:NixOS/nixpkgs/gnome";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, nixpkgs-gnome, flake-utils, ... }@inputs:
    (flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;

            overlays = [ (final: prev: {
              gtuber = prev.stdenv.mkDerivation {
                name = "gtuber";
                version = "0.4.0";

                src = prev.fetchFromGitHub {
                  owner = "Rafostar";
                  repo = "gtuber";
                  rev = "f5a64d442c18ae2fa732a851ccb4859c8a45d944";
                  hash = "sha256-Ua7wUttF7iBsP00iAV5VQiUP/svoybLOIH2vOi33cAs=";
                };

                nativeBuildInputs = [
                  prev.meson
                  prev.ninja
                  prev.cmake
                  prev.pkg-config
                  prev.glib
                  prev.libsoup_3
                  prev.gobject-introspection
                  prev.vala
                  prev.json-glib
                  prev.libxml2
                  prev.gst_all_1.gstreamer prev.gst_all_1.gst-plugins-base prev.gst_all_1.gst-plugins-good prev.gst_all_1.gst-plugins-bad
                ];
                buildInputs = [
                  prev.glib-networking
                ];
              };
            })];
          };
          pkgs-gnome = import nixpkgs-gnome {
            inherit system;
            overlays = [ (final: prev: {
              # Not yet released patches required, otherwise it would panic for certain usage regarding breakpoints.
              blueprint-compiler = prev.blueprint-compiler.overrideAttrs (old: {
                src = prev.fetchFromGitLab {
                  domain = "gitlab.gnome.org";
                  owner = "jwestman";
                  repo = "blueprint-compiler";
                  rev = "v0.14.0";
                  hash = "sha256-pkbTxCN7LagIbOtpiUCkh40aHw6uRtalQVFa47waXjU=";
                };
              });
              libadwaita = prev.libadwaita.overrideAttrs (old: {
                src = prev.fetchFromGitLab {
                  domain = "gitlab.gnome.org";
                  owner = "GNOME";
                  repo = "libadwaita";
                  rev = "8c01db80be540aeeadfaf15b5c3a87e2900f55c1";
                  hash = "sha256-S3SfCdTg/XPBbmLmx6a5pUFtFJ/m5o8pHCpLloVeA7g=";
                };
              });
            })];
          };
          name = "pipeline";
          legacyname = "tubefeeder";
          appid = "de.schmidhuberj.tubefeeder";
        in
        rec { 
          packages.default = 
            with pkgs;
            stdenv.mkDerivation rec {
              cargoDeps = rustPlatform.importCargoLock {
                lockFile = ./Cargo.lock;

                outputHashes = {
                  "clapper-0.1.0" = "sha256-IFFqfSq2OpzfopQXSYfnJ68HGLY+rvcLqk7NTdDd+28=";
                  "pipeline-api-0.1.0" = "sha256-h094ZAJOqX9QC1EUAtzIVztudhndXglkYLcFbH/mpqQ=";
                  "pipeline-core-0.1.0" = "sha256-h094ZAJOqX9QC1EUAtzIVztudhndXglkYLcFbH/mpqQ=";
                  "pipeline-provider-piped-0.1.0" = "sha256-xMcYShrapusoICvZDiUuPLlu7HqYT2+P97oyWHSrLco=";
                  "pipeline-provider-peertube-0.1.0" = "sha256-xMcYShrapusoICvZDiUuPLlu7HqYT2+P97oyWHSrLco=";
                  "piped-openapi-sdk-1.0.0" = "sha256-UFzMYYqCzO6KyJvjvK/hBJtz3FOuSC2gWjKp72WFEGk=";
                };
              };
              src = let fs = lib.fileset; in fs.toSource {
                root = ./.;
                fileset =
                  fs.difference
                    ./.
                    (fs.unions [
                      (fs.maybeMissing ./result)
                      (fs.maybeMissing ./build)
                      ./flake.nix
                      ./flake.lock
                    ]);
              };

              buildInputs = with pkgs; [ pkgs-gnome.libadwaita pkgs-gnome.clapper pkgs-gnome.gst_all_1.gstreamer pkgs-gnome.gst_all_1.gst-plugins-base pkgs-gnome.gst_all_1.gst-plugins-good pkgs-gnome.gst_all_1.gst-plugins-bad gtuber pkgs-gnome.glib-networking sqlite ];
              nativeBuildInputs = with pkgs; [ pkgs-gnome.wrapGAppsHook4 rustPlatform.cargoSetupHook meson gettext pkgs-gnome.glib pkg-config desktop-file-utils appstream ninja rustc cargo openssl pkgs-gnome.blueprint-compiler pkgs-gnome.glib-networking ];

              inherit name;
            };
          devShells.default =
            let 
              run = pkgs.writeShellScriptBin "run" ''
                meson compile -C build && ./build/target/debug/${legacyname}
              '';
              debug = pkgs.writeShellScriptBin "debug" ''
                meson compile -C build && gdb ./build/target/debug/${legacyname}
              '';
              check = pkgs.writeShellScriptBin "check" ''
                cargo clippy
              '';
              format = pkgs.writeShellScriptBin "format" ''
                cargo fmt
                python3 -m json.tool build-aux/${appid}.json build-aux/${appid}.json
                xmllint --format --recover data/resources/resources.gresource.xml -o data/resources/resources.gresource.xml
                xmllint --format --recover data/${appid}.gschema.xml -o data/${appid}.gschema.xml
                xmllint --format --recover data/${appid}.metainfo.xml -o data/${appid}.metainfo.xml
              '';
              prof = pkgs.writeShellScriptBin "prof" ''
                meson compile -C build
                sysprof-cli --force --no-battery --use-trace-fd --speedtrack --gtk $@ tubefeeder.syscap -- ./build/target/debug/${legacyname}
              '';
            in
            with pkgs;
            pkgs.mkShell {
              src = ./.;
              buildInputs = self.packages.${system}.default.buildInputs ++ [ pkgs-gnome.glib-networking ];
              nativeBuildInputs = self.packages.${system}.default.nativeBuildInputs ++ [ rustfmt python3 libxml2 gdb cargo-deny clippy pkgs-gnome.glib-networking sysprof ] ++ [ run check format debug prof ];
              shellHook = ''
                # export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${pkgs-gnome.glib-networking}/lib/gio/modules
                meson setup -Dprofile=development build
              '';
              GSETTINGS_SCHEMA_DIR = "${pkgs-gnome.gtk4}/share/gsettings-schemas/${pkgs-gnome.gtk4.name}/glib-2.0/schemas/:${pkgs-gnome.gsettings-desktop-schemas}/share/gsettings-schemas/${pkgs-gnome.gsettings-desktop-schemas.name}/glib-2.0/schemas/:./build/data/";
              GIO_MODULE_DIR="${pkgs-gnome.glib-networking}/lib/gio/modules/";

              GDK_PIXBUF_MODULE_FILE="${pkgs-gnome.gnome._gdkPixbufCacheBuilder_DO_NOT_USE { 
                                extraLoaders = [ 
                                  pkgs.libjxl 
                                  pkgs.librsvg 
                                  pkgs.webp-pixbuf-loader 
                                  pkgs.libheif.out 
                                ]; 
                              }}";

              RUSTFLAGS="-C force-frame-pointers=yes";
            };
          apps.default = {
            type = "app";
            inherit name;
            program = "${self.packages.${system}.default}/bin/${name}";
          };

          # Note: This may only be run interactively as this requires network access.
          # It requires manually picking a channel to display, waiting one minute for the screenshot to be taken, selecting a video to play and waiting another minute.
          # It furthermore requires a `screenshot-aux` directory containing the `data.sqlite` and `~/.cache/tubefeeder` (renamed to `cache`) which will be copied to the VM.
          packages.makeScreenshot =
            let
              nixos-lib = import (nixpkgs + "/nixos/lib") { };
            in
            nixos-lib.runTest {
              name = "screenshot";
              hostPkgs = pkgs;
              imports = [
                {
                  nodes = {
                    machine = { pkgs, ... }: {
                      boot.loader.systemd-boot.enable = true;
                      boot.loader.efi.canTouchEfiVariables = true;

                      services.xserver.enable = true;
                      services.xserver.displayManager.gdm.enable = true;
                      services.xserver.desktopManager.gnome.enable = true;
                      services.displayManager.autoLogin.enable = true;
                      services.displayManager.autoLogin.user = "alice";

                      users.users.alice = {
                        isNormalUser = true;
                        extraGroups = [ "wheel" ];
                        uid = 1000;
                      };

                      system.stateVersion = "22.05";

                      virtualisation.memorySize = 4096;

                      environment.systemPackages = [
                        self.packages.${system}.default
                      ];

                      environment.sessionVariables = {
                        RUST_LOG="tubefeeder=trace";
                      };

                      systemd.user.services = {
                        "org.gnome.Shell@wayland" = {
                          serviceConfig = {
                            ExecStart = [
                              ""
                              "${pkgs.gnome-shell}/bin/gnome-shell"
                            ];
                          };
                        };
                      };
                    };
                  };

                  testScript = { nodes, ... }:
                    let
                      lib = pkgs.lib;
                      l = lib.lists;

                      user = nodes.machine.users.users.alice;
                      username = user.name;

                      type = word: "machine.send_chars(\"${word}\")";
                      key = key: "machine.send_key(\"${key}\")";
                      sleep = duration: "machine.sleep(${toString duration})";

                      execution = [
                        (sleep 60)
                        (key "alt-print") # XXX: This for some reason sometimes fails. No idea why.
                        "machine.execute(\"mv /home/${username}/Pictures/Screenshots/* screenshot-channel.png\")"
                        "machine.copy_from_vm(\"screenshot-channel.png\", \".\")"

                        (sleep 60)
                        (key "alt-print") # XXX: This for some reason sometimes fails. No idea why.
                        "machine.execute(\"mv /home/${username}/Pictures/Screenshots/* screenshot-video.png\")"
                        "machine.copy_from_vm(\"screenshot-video.png\", \".\")"
                      ];


                      preExecution = [
                        (sleep 20)
                        "machine.copy_from_host(\"./screenshot-aux/data.sqlite\", \"/home/alice/.local/share/tubefeeder/data.sqlite\")"
                        "machine.copy_from_host(\"./screenshot-aux/cache\", \"/home/alice/.cache/tubefeeder\")"
                        (type "Pipeline")
                        (key "ret")
                      ];

                      fullExecution = l.flatten [preExecution (sleep 5) execution ];

                      code = lib.concatStringsSep "\nmachine.sleep(1)\n" fullExecution;
                    in
                      code;
                }
              ];
            };
        })
    );
}
