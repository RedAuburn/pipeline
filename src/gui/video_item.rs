use gtk::glib::Object;

gtk::glib::wrapper! {
    pub struct VideoItem(ObjectSubclass<imp::VideoItem>)
        @extends gtk::Box, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget;
}

impl Default for VideoItem {
    fn default() -> Self {
        Self::new()
    }
}

impl VideoItem {
    pub fn new() -> Self {
        let s: Self = Object::builder::<Self>().build();
        s
    }
}

pub mod imp {
    use std::cell::RefCell;

    use gdk::gio;
    use gdk::gio::Settings;
    use gdk::gio::SimpleAction;
    use gdk::gio::SimpleActionGroup;
    use glib::clone;
    use glib::subclass::InitializingObject;
    use glib::subclass::Signal;
    use glib::Properties;
    use gtk::glib;
    use gtk::prelude::*;
    use gtk::subclass::prelude::*;
    use gtk::CompositeTemplate;
    use gtk::PopoverMenu;
    use once_cell::sync::Lazy;

    use crate::backend::Client;
    use crate::backend::PlayType;
    use crate::backend::Video;
    use crate::config::APP_ID;
    use crate::gui::utility::Utility;

    #[derive(CompositeTemplate, Properties)]
    #[template(resource = "/ui/video_item.ui")]
    #[properties(wrapper_type = super::VideoItem)]
    pub struct VideoItem {
        #[template_child]
        popover_menu: TemplateChild<PopoverMenu>,

        #[property(get, set)]
        video: RefCell<Option<Video>>,

        #[property(get, set)]
        client: RefCell<Client>,

        settings: Settings,
    }

    impl Default for VideoItem {
        fn default() -> Self {
            Self {
                popover_menu: Default::default(),
                video: Default::default(),
                client: Default::default(),
                settings: gtk::gio::Settings::new(APP_ID),
            }
        }
    }

    #[gtk::template_callbacks]
    impl VideoItem {
        #[template_callback]
        pub fn handle_right_clicked(&self) {
            self.popover_menu.set_menu_model(Some(&self.create_menu()));
            self.popover_menu.popup();
        }

        fn setup_actions(&self) {
            let obj = self.obj();
            let action_watch_later = SimpleAction::new("watch-later", None);
            action_watch_later.connect_activate(clone!(
                #[strong]
                obj,
                move |_, _| {
                    if let Some(video) = obj.video() {
                        obj.client().toggle_watch_later(video);
                    }
                }
            ));

            let action_play_external = SimpleAction::new("play-inverted", None);
            action_play_external.connect_activate(clone!(
                #[strong]
                obj,
                move |_, _| {
                    obj.emit_by_name::<()>("play-video", &[&obj.video(), &PlayType::Inverted]);
                }
            ));

            let action_copy_video_url = SimpleAction::new("copy-video-url", None);
            action_copy_video_url.connect_activate(clone!(
                #[strong]
                obj,
                move |_, _| {
                    obj.emit_by_name::<()>("copy-video-url", &[&obj.video()]);
                }
            ));
            let action_download = SimpleAction::new("download", None);
            action_download.connect_activate(clone!(
                #[strong]
                obj,
                move |_, _| {
                    obj.emit_by_name::<()>("download", &[&obj.video()]);
                }
            ));

            let actions = SimpleActionGroup::new();
            obj.insert_action_group("video-item", Some(&actions));
            actions.add_action(&action_watch_later);
            actions.add_action(&action_play_external);
            actions.add_action(&action_copy_video_url);
            actions.add_action(&action_download);
        }

        fn create_menu(&self) -> gio::Menu {
            let menu = gio::Menu::new();
            if self.obj().video().is_some_and(|v| v.watch_later()) {
                menu.append(
                    Some(&gettextrs::gettext("Remove from Watch Later")),
                    Some("video-item.watch-later"),
                );
            } else {
                menu.append(
                    Some(&gettextrs::gettext("Watch Later")),
                    Some("video-item.watch-later"),
                );
            }

            if self.settings.boolean("play-external-default") {
                menu.append(
                    Some(&gettextrs::gettext("Play with Internal Player")),
                    Some("video-item.play-inverted"),
                );
            } else {
                menu.append(
                    Some(&gettextrs::gettext("Play with External Player")),
                    Some("video-item.play-inverted"),
                );
            }

            menu.append(
                Some(&gettextrs::gettext("Copy Video URL")),
                Some("video-item.copy-video-url"),
            );

            menu.append(
                Some(&gettextrs::gettext("Download")),
                Some("video-item.download"),
            );

            // TODO: Download
            // TODO: Open in browser

            menu
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for VideoItem {
        const NAME: &'static str = "TFVideoItem";
        type Type = super::VideoItem;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
            Utility::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for VideoItem {
        fn constructed(&self) {
            self.parent_constructed();
            self.setup_actions();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                vec![
                    Signal::builder("play-video")
                        .param_types([Video::static_type(), PlayType::static_type()])
                        .build(),
                    Signal::builder("copy-video-url")
                        .param_types([Video::static_type()])
                        .build(),
                    Signal::builder("download")
                        .param_types([Video::static_type()])
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for VideoItem {}
    impl BoxImpl for VideoItem {}
}
