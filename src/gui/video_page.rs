use std::borrow::Borrow;

use clapper::MediaItem;
use gdk::prelude::SettingsExt;
use glib::{clone, object::Cast, subclass::types::ObjectSubclassIsExt};
use gtk::prelude::WidgetExt;

use crate::{backend::Video, gspawn, gui::utility::Utility};

use super::window::Window;

gtk::glib::wrapper! {
    pub struct VideoPage(ObjectSubclass<imp::VideoPage>)
        @extends adw::Bin, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget;
}

impl VideoPage {
    pub fn play(&self) {
        self.set_external(false);
        if let Some(video) = self.video() {
            let imp = self.imp();
            // Play with internal clapper
            let player = imp
                .clapper
                .player()
                .expect("Clapper does not have a player");
            let queue = player.queue().expect("Clapper does not have a queue");
            queue.clear();
            queue.add_item(&MediaItem::new(&Self::convert_to_clapper_playable_url(
                &video,
            )));
            player.play();
        } else {
            self.pause();
        }
    }

    fn play_pause(&self) {
        let player = self
            .imp()
            .clapper
            .player()
            .expect("Clapper does not have a player");
        match player.state() {
            clapper::PlayerState::Paused => player.play(),
            clapper::PlayerState::Playing => player.pause(),
            _ => {}
        }
    }

    pub fn play_external(&self) {
        self.pause();
        self.set_external(true);

        if let Some(video) = self.video() {
            let url = video.playable_url();
            let imp = self.imp();

            let player = imp.settings.string("player");

            gspawn!(clone!(
                #[strong(rename_to = s)]
                self,
                async move {
                    let res = Utility::run_command_on_url(player, url).await;
                    if let Err(e) = res {
                        s.window()
                            .display_error(gettextrs::gettext("Failed to spawn video player"), e);
                    }
                }
            ));
        }
    }

    pub fn pause(&self) {
        // Stop playing.
        let player = self
            .imp()
            .clapper
            .player()
            .expect("Clapper does not have a player");
        player.pause();
    }

    fn window(&self) -> Window {
        self.root().unwrap().dynamic_cast().unwrap()
    }

    fn convert_to_clapper_playable_url(video: &Video) -> String {
        let url = video.playable_url().to_string();
        match video.id().platform.borrow() {
            "peertube" => {
                // This would be better expressed as `Url::set_scheme`, but this would error as `http(s)` is "special" and `peertube` is not.
                if let Some((_, rest)) = url.split_once("://") {
                    format!("peertube://{}", rest)
                } else {
                    url
                }
            }
            _ => url,
        }
    }
}

pub mod imp {
    use std::cell::Cell;
    use std::cell::RefCell;

    use adw::subclass::bin::BinImpl;
    use clapper::Mpris;
    use glib::clone;
    use glib::subclass::InitializingObject;
    use glib::subclass::Signal;
    use glib::Propagation;
    use glib::Properties;
    use gtk::glib;
    use gtk::prelude::*;
    use gtk::subclass::prelude::*;
    use gtk::Align;
    use gtk::CallbackAction;
    use gtk::CompositeTemplate;
    use gtk::Shortcut;
    use gtk::ShortcutController;
    use gtk::ShortcutTrigger;
    use once_cell::sync::Lazy;

    use crate::backend::Channel;
    use crate::backend::Client;
    use crate::backend::Video;
    use crate::config::APP_ID;
    use crate::gspawn;
    use crate::gui::utility::Utility;

    #[derive(CompositeTemplate, Properties)]
    #[template(resource = "/ui/video_page.ui")]
    #[properties(wrapper_type = super::VideoPage)]
    pub struct VideoPage {
        #[template_child]
        pub(super) clapper: TemplateChild<clapper_gtk::Video>,
        #[property(get, set = Self::set_video, nullable)]
        video: RefCell<Option<Video>>,

        #[property(get, set)]
        fullscreened: Cell<bool>,

        #[property(get, set)]
        external: Cell<bool>,

        #[property(get, set)]
        client: RefCell<Client>,

        pub settings: gtk::gio::Settings,
    }

    impl Default for VideoPage {
        fn default() -> Self {
            Self {
                clapper: Default::default(),
                video: Default::default(),
                fullscreened: Default::default(),
                external: Default::default(),
                client: Default::default(),
                settings: gtk::gio::Settings::new(APP_ID),
            }
        }
    }

    impl VideoPage {
        // Recommended syntax is ugly and the item will be moved to `pedantic` soon.
        #[allow(clippy::assigning_clones)]
        fn set_video(&self, video: Option<Video>) {
            *self.video.borrow_mut() = video.clone();

            if let Some(video) = video {
                let obj = self.obj();
                let client = obj.client();

                // Load additional data
                gspawn!(clone!(
                    #[strong]
                    obj,
                    #[strong]
                    client,
                    #[strong]
                    video,
                    async move {
                        let result = client.populate_video_information(&video.id()).await;
                        if let Err(e) = result {
                            log::error!("Failed to populate video information: {}", e);
                            obj.window().display_error(
                                gettextrs::gettext("Failed to populate video information"),
                                Box::new(e),
                            );
                        }

                        let result = client
                            .populate_channel_information(&video.uploader().id())
                            .await;
                        if let Err(e) = result {
                            log::error!("Failed to populate channel information: {}", e);
                            obj.window().display_error(
                                gettextrs::gettext("Failed to fetch channel information"),
                                Box::new(e),
                            );
                        }
                    }
                ));
            }
        }
    }

    #[gtk::template_callbacks]
    impl VideoPage {
        #[template_callback(function)]
        fn fullscreen_to_valign(fullscreened: bool) -> Align {
            if fullscreened {
                Align::Fill
            } else {
                Align::Start
            }
        }

        #[template_callback(function)]
        fn format_subscribers(subscribers: u64) -> String {
            gettextrs::ngettext(
                "one subscriber",
                "{} subscribers",
                subscribers.try_into().unwrap_or_default(),
            )
            .replace("{}", &Utility::format_number_compact(subscribers))
        }

        #[template_callback(function)]
        fn format_uploaded(uploaded: String) -> String {
            gettextrs::gettext("Uploaded {}").replace("{}", &uploaded)
        }

        #[template_callback]
        fn display_channel(&self) {
            let obj = self.obj();
            if let Some(channel) = obj.video().map(|v| v.uploader()) {
                obj.emit_by_name::<()>("display-channel", &[&channel]);
            }
        }

        #[template_callback]
        fn toggle_fullscreen(&self) {
            let obj = self.obj();
            obj.set_fullscreened(!obj.fullscreened());
        }

        #[template_callback]
        fn download(&self) {
            let obj = self.obj();
            if let Some(video) = obj.video() {
                obj.emit_by_name::<()>("download-video", &[&video]);
            }
        }

        #[template_callback]
        fn copy_to_clipboard(&self) {
            let obj = self.obj();
            if let Some(video) = obj.video() {
                obj.emit_by_name::<()>("copy-video-url-to-clipboard", &[&video]);
            }
        }

        #[template_callback(function)]
        fn watch_later_icon_name(is_watch_later: bool) -> &'static str {
            if is_watch_later {
                "bookmark-toggled-symbolic"
            } else {
                "bookmark-untoggled-symbolic"
            }
        }

        #[template_callback(function)]
        fn watch_later_tooltip_text(is_watch_later: bool) -> String {
            if is_watch_later {
                gettextrs::gettext("Remove video from Watch Later")
            } else {
                gettextrs::gettext("Add video to Watch Later")
            }
        }

        fn setup(&self) {
            let obj = self.obj();
            let shortcut_escape = Shortcut::new(
                Some(
                    ShortcutTrigger::parse_string("Escape")
                        .expect("Escape to be a valid shortcut trigger"),
                ),
                Some(CallbackAction::new(clone!(
                    #[weak]
                    obj,
                    #[upgrade_or_panic]
                    move |_, _| {
                        if obj.fullscreened() {
                            obj.set_fullscreened(false);
                            Propagation::Stop
                        } else {
                            obj.pause();
                            Propagation::Proceed
                        }
                    }
                ))),
            );

            let shortcut_controller = ShortcutController::new();
            shortcut_controller.add_shortcut(shortcut_escape);
            obj.add_controller(shortcut_controller);

            // Set up MPRIS
            let player = self.clapper.player().expect("Clapper to have a Player");
            let mpris = Mpris::new(
                "org.mpris.MediaPlayer2.tubefeeder",
                &gettextrs::gettext("Pipeline"),
                Some("de.schmidhuberj.tubefeeder"),
            );

            player.add_feature(&mpris);

            // Set up player settings
            let settings = &self.settings;
            settings.bind("player-volume", &player, "volume").build();
            settings.bind("player-speed", &player, "speed").build();
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for VideoPage {
        const NAME: &'static str = "TFVideoPage";
        type Type = super::VideoPage;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            clapper_gtk::Video::ensure_type();
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
            Utility::bind_template_callbacks(klass);

            klass.add_shortcut(&Shortcut::new(
                Some(ShortcutTrigger::parse_string("space").expect("Space to be a valid shortcut")),
                Some(CallbackAction::new(|obj, _| {
                    let obj: &super::VideoPage =
                        obj.dynamic_cast_ref().expect("Widget to be a VideoPage");
                    obj.play_pause();
                    Propagation::Stop
                })),
            ))
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for VideoPage {
        fn constructed(&self) {
            self.parent_constructed();
            self.setup();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                vec![
                    Signal::builder("display-channel")
                        .param_types([Channel::static_type()])
                        .build(),
                    Signal::builder("download-video")
                        .param_types([Video::static_type()])
                        .build(),
                    Signal::builder("copy-video-url-to-clipboard")
                        .param_types([Video::static_type()])
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for VideoPage {
        fn grab_focus(&self) -> bool {
            self.clapper.grab_focus()
        }
    }
    impl BinImpl for VideoPage {}
}
