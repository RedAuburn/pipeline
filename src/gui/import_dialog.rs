use gdk::glib::Object;
use glib::object::Cast;
use gtk::prelude::WidgetExt;

use crate::backend::Client;

use super::window::Window;

gtk::glib::wrapper! {
    pub struct ImportDialog(ObjectSubclass<imp::ImportDialog>)
        @extends adw::Dialog, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl ImportDialog {
    pub fn new(client: Client) -> Self {
        Object::builder().property("client", client).build()
    }

    fn window(&self) -> Window {
        self.root().unwrap().dynamic_cast().unwrap()
    }
}

pub mod imp {
    use std::cell::RefCell;

    use adw::prelude::AdwDialogExt;
    use adw::subclass::prelude::AdwDialogImpl;
    use gdk::gio::Cancellable;
    use glib::subclass::InitializingObject;
    use glib::{clone, Properties};
    use gtk::prelude::ObjectExt;
    use gtk::subclass::prelude::*;
    use gtk::{glib, template_callbacks};
    use gtk::{CompositeTemplate, FileDialog, FileFilter};

    use crate::backend::Client;

    #[derive(CompositeTemplate, Default, Properties)]
    #[properties(wrapper_type = super::ImportDialog)]
    #[template(resource = "/ui/import_dialog.ui")]
    pub struct ImportDialog {
        #[property(get, set)]
        client: RefCell<Client>,
    }

    #[template_callbacks]
    impl ImportDialog {
        #[template_callback]
        fn import_youtube(&self) {
            let obj = self.obj();
            let window = obj.window();

            let csv_filter = FileFilter::new();
            csv_filter.add_mime_type("text/csv");
            let file_dialog = FileDialog::builder()
                .accept_label(gettextrs::gettext("Import"))
                .default_filter(&csv_filter)
                .title(gettextrs::gettext("Import YouTube Subscriptions"))
                .build();
            file_dialog.open(
                Some(&window),
                None::<&Cancellable>,
                clone!(
                    #[weak]
                    obj,
                    #[weak]
                    window,
                    move |res| {
                        if let Ok(file) = res {
                            if let Err(e) = obj.client().import_youtube(file) {
                                window.display_error(
                                    gettextrs::gettext("Error importing YouTube subscriptions"),
                                    e,
                                );
                            }
                        }

                        obj.close();
                    }
                ),
            )
        }
        #[template_callback]
        fn import_newpipe(&self) {
            let obj = self.obj();
            let window = obj.window();

            let csv_filter = FileFilter::new();
            csv_filter.add_mime_type("application/json");
            csv_filter.add_mime_type("application/vnd.document+json");
            let file_dialog = FileDialog::builder()
                .accept_label(gettextrs::gettext("Import"))
                .default_filter(&csv_filter)
                .title(gettextrs::gettext("Import NewPipe Subscriptions"))
                .build();
            file_dialog.open(
                Some(&window),
                None::<&Cancellable>,
                clone!(
                    #[weak]
                    obj,
                    #[weak]
                    window,
                    move |res| {
                        if let Ok(file) = res {
                            if let Err(e) = obj.client().import_newpipe(file) {
                                window.display_error(
                                    gettextrs::gettext("Error importing NewPipe subscriptions"),
                                    e,
                                );
                            }
                        }

                        obj.close();
                    }
                ),
            )
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ImportDialog {
        const NAME: &'static str = "TFImportDialog";
        type Type = super::ImportDialog;
        type ParentType = adw::Dialog;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ImportDialog {}
    impl WidgetImpl for ImportDialog {}
    impl AdwDialogImpl for ImportDialog {}
}
