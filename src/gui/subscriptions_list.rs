gtk::glib::wrapper! {
    pub struct SubscriptionsList(ObjectSubclass<imp::SubscriptionsList>)
        @extends adw::Bin, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget;
}

pub mod imp {
    use adw::subclass::bin::BinImpl;
    use glib::clone;
    use glib::subclass::InitializingObject;
    use glib::subclass::Signal;
    use glib::Properties;
    use gtk::glib;
    use gtk::prelude::*;
    use gtk::subclass::prelude::*;
    use gtk::GridView;
    use gtk::SignalListItemFactory;
    use gtk::Widget;
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    use gtk::CompositeTemplate;

    use crate::backend::Channel;
    use crate::backend::Client;
    use crate::backend::SubscriptionsStore;
    use crate::gui::channel_item::ChannelItem;

    #[derive(CompositeTemplate, Properties, Default)]
    #[template(resource = "/ui/subscriptions_list.ui")]
    #[properties(wrapper_type = super::SubscriptionsList)]
    pub struct SubscriptionsList {
        #[template_child]
        list: TemplateChild<GridView>,

        #[property(get, set = Self::set_model)]
        model: RefCell<Option<SubscriptionsStore>>,
        #[property(get, set, nullable)]
        selected: RefCell<Option<Channel>>,

        #[property(get, set)]
        client: RefCell<Client>,
    }

    impl SubscriptionsList {
        fn set_model(&self, model: SubscriptionsStore) {
            self.obj()
                .property_expression("selected")
                .bind(&model, "selected", Widget::NONE);
            self.model.borrow_mut().replace(model);
        }

        pub(super) fn setup(&self) {
            let obj = self.obj();

            let factory = SignalListItemFactory::new();
            factory.connect_setup(clone!(
                #[strong]
                obj,
                move |_, list_item| {
                    let list_item = list_item.downcast_ref::<gtk::ListItem>().unwrap();
                    let channel_item = ChannelItem::new();
                    list_item.set_child(Some(&channel_item));

                    list_item.property_expression("item").bind(
                        &channel_item,
                        "channel",
                        Widget::NONE,
                    );
                    obj.property_expression("client")
                        .bind(&channel_item, "client", Widget::NONE);
                }
            ));
            self.list.set_factory(Some(&factory));

            self.list.set_single_click_activate(true);
            self.list.connect_activate(clone!(
                #[strong]
                obj,
                move |list_view, position| {
                    let model = list_view.model().expect("The model has to exist.");
                    let channel = model
                        .item(position)
                        .expect("The item has to exist.")
                        .downcast::<Channel>()
                        .expect("The item has to be an `Channel`.");

                    obj.emit_by_name::<()>("display-channel", &[&channel]);
                }
            ));
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SubscriptionsList {
        const NAME: &'static str = "TFSubscriptionsList";
        type Type = super::SubscriptionsList;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for SubscriptionsList {
        fn constructed(&self) {
            self.parent_constructed();
            self.setup();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                vec![Signal::builder("display-channel")
                    .param_types([Channel::static_type()])
                    .build()]
            });
            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for SubscriptionsList {}
    impl BinImpl for SubscriptionsList {}
}
