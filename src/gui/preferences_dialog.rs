use gdk::glib::Object;

gtk::glib::wrapper! {
    pub struct PreferencesDialog(ObjectSubclass<imp::PreferencesDialog>)
        @extends adw::PreferencesDialog, adw::Dialog, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl Default for PreferencesDialog {
    fn default() -> Self {
        Self::new()
    }
}

impl PreferencesDialog {
    pub fn new() -> Self {
        Object::builder().build()
    }
}

pub mod imp {
    use std::cell::Cell;
    use std::rc::Rc;

    use adw::prelude::ActionRowExt;
    use adw::prelude::EntryRowExt;
    use adw::prelude::PreferencesRowExt;
    use adw::subclass::prelude::AdwDialogImpl;
    use adw::subclass::prelude::PreferencesDialogImpl;
    use adw::ActionRow;
    use adw::EntryRow;
    use adw::SwitchRow;
    use gdk::gio::Settings;
    use gdk::gio::SettingsBindFlags;
    use gdk::prelude::DragExt;
    use gdk::prelude::SettingsExt;
    use gdk::prelude::SettingsExtManual;
    use gdk::ContentProvider;
    use gdk::DragAction;
    use glib::clone;
    use glib::object::Cast;
    use glib::object::CastNone;
    use glib::object::ObjectExt;
    use glib::subclass::InitializingObject;
    use glib::types::StaticType;
    use glib::value::ToValue;
    use gtk::glib;
    use gtk::prelude::ButtonExt;
    use gtk::prelude::EditableExt;
    use gtk::prelude::ListBoxRowExt;
    use gtk::prelude::WidgetExt;
    use gtk::subclass::prelude::*;
    use gtk::Align;
    use gtk::Button;
    use gtk::CompositeTemplate;
    use gtk::DragIcon;
    use gtk::DragSource;
    use gtk::DropControllerMotion;
    use gtk::DropTarget;
    use gtk::Image;
    use gtk::InputHints;
    use gtk::ListBox;
    use gtk::ListBoxRow;
    use gtk::StateFlags;
    use url::Url;

    #[derive(CompositeTemplate)]
    #[template(resource = "/ui/preferences_dialog.ui")]
    pub struct PreferencesDialog {
        #[template_child]
        switch_external_default: TemplateChild<SwitchRow>,
        #[template_child]
        entry_external_player: TemplateChild<EntryRow>,
        #[template_child]
        entry_external_downloader: TemplateChild<EntryRow>,

        #[template_child]
        switch_reload_on_startup: TemplateChild<SwitchRow>,
        #[template_child]
        piped_api_urls: TemplateChild<ListBox>,

        #[template_child]
        entry_peertube_search_api: TemplateChild<EntryRow>,

        settings: Settings,
    }

    #[gtk::template_callbacks]
    impl PreferencesDialog {
        fn init_settings(&self) {
            self.settings
                .bind(
                    "reload-on-startup",
                    &self.switch_reload_on_startup.get(),
                    "active",
                )
                .flags(SettingsBindFlags::DEFAULT)
                .build();
            self.settings
                .bind(
                    "play-external-default",
                    &self.switch_external_default.get(),
                    "active",
                )
                .flags(SettingsBindFlags::DEFAULT)
                .build();

            self.entry_peertube_search_api
                .set_text(&self.settings.string("peertube-search-api"));
            self.entry_external_player
                .set_text(&self.settings.string("player"));
            self.entry_external_downloader
                .set_text(&self.settings.string("downloader"));
        }

        fn create_piped_row(&self, api: Url) -> ActionRow {
            // Inspired by <https://github.com/workbenchdev/demos/blob/f0e3559c0f7cfd8a083bac51d17374f9460d624d/src/Drag%20and%20Drop/main.py>.
            let row = ActionRow::builder().title(api.to_string()).build();

            // Source of drag.
            // TODO: Does not seem to work.
            let xy: Rc<Cell<(f64, f64)>> = Rc::new(Cell::new((0.0, 0.0)));

            let icon = Image::from_icon_name("list-drag-handle-symbolic");
            icon.add_css_class("dim-label");
            row.add_prefix(&icon);

            let remove_button = Button::from_icon_name("user-trash-symbolic");
            remove_button.set_valign(Align::Center);
            remove_button.add_css_class("flat");
            remove_button.connect_clicked(clone!(
                #[weak(rename_to = s)]
                self,
                #[weak(rename_to = list)]
                self.piped_api_urls,
                #[weak]
                row,
                move |_| {
                    list.remove(&row);
                    s.piped_apis_updated();
                }
            ));
            row.add_suffix(&remove_button);

            let drop_controller = DropControllerMotion::new();
            let drag_source = DragSource::builder().actions(DragAction::MOVE).build();

            drag_source.connect_prepare(clone!(
                #[weak]
                row,
                #[strong]
                xy,
                #[upgrade_or_default]
                move |_, x, y| {
                    xy.replace((x, y));
                    Some(ContentProvider::for_value(&row.to_value()))
                }
            ));

            drag_source.connect_drag_begin(clone!(
                #[weak]
                row,
                #[strong]
                xy,
                move |_, drag| {
                    let drag_widget = ListBox::new();
                    drag_widget.set_size_request(row.width(), row.height());
                    drag_widget.add_css_class("boxed-list");

                    let drag_row = ActionRow::builder().title(row.title()).build();
                    let icon = Image::from_icon_name("list-drag-handle-symbolic");
                    icon.add_css_class("dim-label");
                    drag_row.add_prefix(&icon);
                    drag_widget.append(&drag_row);
                    drag_widget.drag_highlight_row(&drag_row);

                    let icon = DragIcon::for_drag(drag);
                    icon.set_child(Some(&drag_widget));

                    let (x, y) = xy.get();
                    drag.set_hotspot(x as i32, y as i32);
                }
            ));

            drop_controller.connect_enter(clone!(
                #[weak]
                row,
                #[weak(rename_to = list)]
                self.piped_api_urls,
                move |_, _, _| {
                    list.drag_highlight_row(&row);
                }
            ));

            drop_controller.connect_leave(clone!(
                #[weak(rename_to = list)]
                self.piped_api_urls,
                move |_| {
                    list.drag_unhighlight_row();
                },
            ));

            row.add_controller(drop_controller);
            row.add_controller(drag_source);

            row
        }

        fn init_piped_preferences_urls(&self) {
            let drop_target = DropTarget::new(ActionRow::static_type(), DragAction::MOVE);

            drop_target.connect_drop(clone!(
                #[weak(rename_to = s)]
                self,
                #[weak(rename_to = list)]
                self.piped_api_urls,
                #[upgrade_or_default]
                move |_, value, _, y| {
                    let Some(target_row) = list.row_at_y(y as i32) else {
                        return false;
                    };
                    if target_row.is::<EntryRow>() {
                        return false;
                    }
                    let target_index = target_row.index();

                    let Ok(row) = value.get::<ListBoxRow>() else {
                        return false;
                    };

                    list.remove(&row);
                    list.insert(&row, target_index);
                    target_row.set_state_flags(StateFlags::NORMAL, true);

                    s.piped_apis_updated();

                    true
                }
            ));

            self.piped_api_urls.add_controller(drop_target);

            let apis: Vec<String> = self.settings.get("piped-api-urls");

            for api in apis {
                if let Ok(url) = Url::parse(&api) {
                    let row = self.create_piped_row(url);
                    self.piped_api_urls.append(&row);
                }
            }

            let create_new_row = EntryRow::builder()
                .title(gettextrs::gettext("New Piped API"))
                .show_apply_button(true)
                .build();
            create_new_row.connect_apply(clone!(
                #[weak(rename_to = s)]
                self,
                move |create_new_row| {
                    let text = create_new_row.text();
                    if let Ok(url) = Url::parse(&text) {
                        let row = s.create_piped_row(url);
                        s.piped_api_urls.remove(create_new_row);
                        s.piped_api_urls.append(&row);
                        s.piped_api_urls.append(create_new_row);
                        create_new_row.remove_css_class("error");
                        create_new_row.set_text("");

                        s.piped_apis_updated();
                    } else {
                        create_new_row.add_css_class("error");
                    }
                }
            ));
            create_new_row.set_input_purpose(gtk::InputPurpose::Url);
            create_new_row.set_input_hints(
                InputHints::NO_SPELLCHECK & InputHints::LOWERCASE & InputHints::NO_EMOJI,
            );

            self.piped_api_urls.append(&create_new_row);

            self.piped_apis_updated();
        }

        fn piped_apis_updated(&self) {
            let mut urls = vec![];

            let mut n_action_rows = 0;
            let mut child = self.piped_api_urls.first_child();
            while let Some(row) = child {
                if let Some(action_row) = row.dynamic_cast_ref::<ActionRow>() {
                    n_action_rows += 1;
                    action_row.set_sensitive(true);
                    urls.push(action_row.title().to_string());
                }

                child = row.next_sibling();
            }

            if n_action_rows == 1 {
                if let Ok(action_row) = self
                    .piped_api_urls
                    .first_child()
                    .and_dynamic_cast::<ActionRow>()
                {
                    action_row.set_sensitive(false);
                }
            }

            let _ = self.settings.set("piped-api-urls", urls);
        }

        #[template_callback]
        fn apply_peertube_search_api(&self) {
            let text = self.entry_peertube_search_api.text();

            if Url::parse(&text).is_ok() {
                self.entry_peertube_search_api.remove_css_class("error");

                let _ = self.settings.set_string("peertube-search-api", &text);
            } else {
                self.entry_peertube_search_api.add_css_class("error");
            }
        }

        #[template_callback]
        fn apply_external_player(&self) {
            let text = self.entry_external_player.text();

            if !text.is_empty() {
                self.entry_external_player.remove_css_class("error");

                let _ = self.settings.set_string("player", &text);
            } else {
                self.entry_external_player.add_css_class("error");
            }
        }

        #[template_callback]
        fn apply_external_downloader(&self) {
            let text = self.entry_external_downloader.text();

            if !text.is_empty() {
                self.entry_external_downloader.remove_css_class("error");

                let _ = self.settings.set_string("downloader", &text);
            } else {
                self.entry_external_downloader.add_css_class("error");
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PreferencesDialog {
        const NAME: &'static str = "TFPreferencesDialog";
        type Type = super::PreferencesDialog;
        type ParentType = adw::PreferencesDialog;

        fn new() -> Self {
            Self {
                switch_external_default: Default::default(),
                entry_external_player: Default::default(),
                entry_external_downloader: Default::default(),
                switch_reload_on_startup: Default::default(),
                piped_api_urls: Default::default(),
                entry_peertube_search_api: Default::default(),
                settings: Settings::new(crate::config::APP_ID),
            }
        }

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PreferencesDialog {
        fn constructed(&self) {
            self.parent_constructed();
            self.init_settings();
            self.init_piped_preferences_urls();
        }
    }
    impl WidgetImpl for PreferencesDialog {}
    impl PreferencesDialogImpl for PreferencesDialog {}
    impl AdwDialogImpl for PreferencesDialog {}
}
