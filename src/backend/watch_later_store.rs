use gdk::gio;
use gdk::prelude::{Cast, ListModelExt};
use gdk::subclass::prelude::ObjectSubclassIsExt;
use glib::Object;

use super::Video;

// TODO: Section model?
gtk::glib::wrapper! {
    pub struct WatchLaterStore(ObjectSubclass<imp::WatchLaterStore>)
        @implements gio::ListModel, gtk::SelectionModel;
}

impl Default for WatchLaterStore {
    fn default() -> Self {
        Object::builder().build()
    }
}

impl WatchLaterStore {
    /// Toggle a video in the list.
    ///
    /// Returns `true` if the video was added to the list, and `false` if it was removed.
    pub fn toggle(&self, video: Video) -> bool {
        let mut videos = self.imp().videos.borrow_mut();

        let index = videos.binary_search_by(|v| v.uploaded().cmp(&video.uploaded()).reverse());
        match index {
            Ok(index) => {
                videos.remove(index);
                drop(videos);
                self.items_changed(index as u32, 1, 0);
                self.notify_is_empty();
                false
            }
            Err(index) => {
                videos.insert(index, video);
                drop(videos);
                self.items_changed(index as u32, 0, 1);
                self.notify_is_empty();
                true
            }
        }
    }

    fn items_changed(&self, position: u32, removed: u32, added: u32) {
        self.upcast_ref::<gio::ListModel>()
            .items_changed(position, removed, added);
    }
}

mod imp {
    use gdk::gio;
    use gdk::prelude::Cast;
    use glib::subclass::types::ObjectSubclassExt;
    use glib::types::StaticType;
    use gtk::glib;
    use gtk::prelude::SelectionModelExt;
    use gtk::subclass::selection_model::SelectionModelImpl;
    use std::cell::RefCell;

    use gdk::subclass::prelude::{DerivedObjectProperties, ListModelImpl};
    use gdk::{
        glib::Properties,
        prelude::ObjectExt,
        subclass::prelude::{ObjectImpl, ObjectSubclass},
    };

    use crate::backend::Video;

    #[derive(Default, Properties)]
    #[properties(wrapper_type = super::WatchLaterStore)]
    pub struct WatchLaterStore {
        #[property(name = "is-empty", type = bool, get = |s: &Self| s.videos.borrow().is_empty())]
        pub(super) videos: RefCell<Vec<Video>>,

        #[property(get, set = Self::set_selected, nullable)]
        selected: RefCell<Option<Video>>,
    }

    impl WatchLaterStore {
        fn set_selected(&self, selected: Option<Video>) {
            self.selected.replace(selected);
            // Don't know where exactly it changed; emit that it changed everywhere.
            let num_videos = self.videos.borrow().len() as u32;
            if num_videos > 0 {
                self.obj().selection_changed(0, num_videos);
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WatchLaterStore {
        const NAME: &'static str = "TFWatchLaterStore";
        type Type = super::WatchLaterStore;
        type Interfaces = (gio::ListModel, gtk::SelectionModel);
    }

    #[glib::derived_properties]
    impl ObjectImpl for WatchLaterStore {}

    impl ListModelImpl for WatchLaterStore {
        fn item_type(&self) -> glib::Type {
            Video::static_type()
        }

        fn n_items(&self) -> u32 {
            self.videos.borrow().len().try_into().unwrap_or_default()
        }

        fn item(&self, position: u32) -> Option<glib::Object> {
            let list = self.videos.borrow();

            list.get(position as usize)
                .map(|o| o.clone().upcast::<glib::Object>())
        }
    }

    impl SelectionModelImpl for WatchLaterStore {
        fn is_selected(&self, position: u32) -> bool {
            let list = self.videos.borrow();
            let selection = self.selected.borrow();
            let Some(selection) = selection.as_ref() else {
                return false;
            };
            list.get(position as usize)
                .is_some_and(|v| v.id() == selection.id())
        }

        fn selection_in_range(&self, position: u32, n_items: u32) -> gtk::Bitset {
            let result = gtk::Bitset::new_range(position, n_items);
            let list = self.videos.borrow();
            let selection = self.selected.borrow();
            let Some(selection) = selection.as_ref() else {
                return result;
            };

            let index: Option<u32> = list
                .iter()
                .position(|v| v.id() == selection.id())
                .and_then(|v| v.try_into().ok());

            if let Some(index) = index {
                if position <= index && index < position + n_items {
                    result.add(position);
                }
            }
            result
        }
    }
}
