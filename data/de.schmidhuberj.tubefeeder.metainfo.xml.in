<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>de.schmidhuberj.tubefeeder</id>
  <project_license>GPL-3.0-or-later</project_license>
  <metadata_license>FSFAP</metadata_license>
  <name>Pipeline</name>
  <summary>Follow video creators</summary>
  <launchable type="desktop-id">de.schmidhuberj.tubefeeder.desktop</launchable>
  <content_rating type="oars-1.1"/>
  <developer_name translate="no">Julian Schmidhuber</developer_name>
  <developer id="de.schmidhuberj.mobile">
    <name translate="no">Julian Schmidhuber</name>
  </developer>
  <translation type="gettext">tubefeeder</translation>
  <update_contact>flatpak@schmidhuberj.de</update_contact>

  <releases>
    <release version="2.0.3" date="2024-11-02">
      <description translate="no">
        <p>Changed</p>
        <ul>
          <li>Improved import dialogs.</li>
          <li>Make the search bar wider.</li>
          <li>Improved error dialog.</li>
          <li>Show that the app is loading when starting the app.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Panic for migrating watch-later list to the new version for some YouTube videos.</li>
          <li>Importing NewPipe videos not showing the file to import.</li>
        </ul>
        <p>Chores</p>
        <ul>
          <li>Updated to GNOME 47 runtimes.</li>
        </ul>
      </description>
    </release>
    <release version="2.0.2" date="2024-09-14">
      <description translate="no">
        <p>HOTFIX</p>
        <ul>
          <li>Bad performance of the built-in video player.</li>
        </ul>
        <p>Added</p>
        <ul>
          <li>Ignore Piped error when opening a video.</li>
          <li>Don't show video title in player when not fullscreened.</li>
        </ul>
      </description>
    </release>
    <release version="2.0.1" date="2024-09-12">
      <description translate="no">
        <p>Fixed</p>
        <ul>
          <li>Infinite loading on channels with no videos.</li>
          <li>Video not stopping when displaying channel in the subscriptions tab.</li>
          <li>Video player not centered on fullscreen when video aspect ratio does not match display aspect ratio.</li>
          <li>Slight redesign of channel page to fix video widgets growing in size when scrolling.</li>
          <li>Swiping not pausing video and exiting fullscreen.</li>
          <li>UI not fitting in window with certain widths.</li>
        </ul>
      </description>
    </release>
    <release version="2.0.0" date="2024-09-10">
      <description translate="no">
        <p>A complete rewrite of the application, in order to allow for easier maintenance, more features and a better user experience.</p>
        <p>Added</p>
        <ul>
          <li>Search for videos and channels.</li>
          <li>Play videos inside the application using Clapper.</li>
          <li>Show toast when the video URL was copied.</li>
          <li>Import PeerTube subscriptions from NewPipe</li>
          <li>Allow for backup Piped instances.</li>
          <li>Option to not automatically refresh when started.</li>
          <li>Infinite scrolling of the feed.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Many videos not showing up in the feed anymore. Workaround for an upstream Piped issue.</li>
        </ul>
      </description>
    </release>
    <release version="1.15.0" date="2024-05-28">
      <description translate="no">
        <p>Fix</p>
        <ul>
          <li>Crash with certain channels.</li>
        </ul>
        <p>Chores</p>
        <ul>
          <li>Updated to GNOME 46 libraries.</li>
        </ul>
      </description>
    </release>
    <release version="1.14.5" date="2024-02-24">
      <description translate="no">
        <p>HOTFIX</p>
        <ul>
          <li>Subscription names not showing up in the subscription list anymore.</li>
        </ul>
      </description>
    </release>
    <release version="1.14.4" date="2024-02-22">
      <description translate="no">
        <p>Added</p>
        <ul>
          <li>Action to open video in the browser.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Outdated window title</li>
          <li>Wrong offset of the reload-spinner in relation to the reload-button.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>Updated selected Piped instance list.</li>
        </ul>
      </description>
    </release>
    <release version="1.14.3" date="2024-01-11">
      <description translate="no">
        <p>Added</p>
        <ul>
          <li>Livi video player in the list of predefined video players.</li>
          <li>Option to hide all short videos.</li>
        </ul>
      </description>
    </release>
    <release version="1.14.2" date="2023-12-08">
      <description translate="no">
        <p>Added</p>
        <ul>
          <li>Video title as tooltip of the video.</li>
          <li>Video title in the video information screen.</li>
          <li>Error handling when failing to add a subscription.</li>
          <li>Error handling when failing to play or download videos.</li>
        </ul>
      </description>
    </release>
    <release version="1.14.1" date="2023-11-11">
      <description translate="no">
        <p>Added</p>
        <ul>
          <li>Keyboard shortcuts for actions.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Video duration placed out-of-bounds if thumbnail does not load.</li>
        </ul>
        <p>Chores</p>
        <ul>
          <li>Updated dependencies.</li>
        </ul>
      </description>
    </release>
    <release version="1.13.1" date="2023-08-24">
      <description translate="no">
        <p>Fixed</p>
        <ul>
          <li>Copy video URL not working anymore.</li>
          <li>Missing accesibility labels.</li>
        </ul>
      </description>
    </release>
    <release version="1.13.0" date="2023-08-20">
      <description translate="no">
        <p>Added</p>
        <ul>
          <li>Dialog showing video information including likes, dislikes (not for YouTube), views and video description.</li>
          <li>Show video duration on video thumbnails.</li>
        </ul>
        <p>Removed</p>
        <ul>
          <li>Removed Lbry support as it will have to shut down soon.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>File chooser dialog for importing videos not working.</li>
        </ul>
      </description>
    </release>
    <release version="1.12.0" date="2023-07-29">
      <description translate="no">
        <p>Update dependencies. Fix issue with some Peertube subscriptions.</p>
      </description>
    </release>
    <release version="1.11.0" date="2023-07-16">
      <description translate="no">
        <p>Rebranding to Pipeline.</p>
      </description>
    </release>
    <release version="1.10.0" date="2023-06-09">
      <description translate="no">
        <p>Load videos in feed automatically and introduce an option to only show videos from yesterday.</p>
      </description>
    </release>
    <release version="1.9.6" date="2023-04-16">
      <description translate="no">
        <p>Hotfix for too large thumbnails.</p>
      </description>
    </release>
    <release version="1.9.5" date="2023-04-15">
      <description translate="no">
        <p>Major UI updates to the feed list.</p>
      </description>
    </release>
    <release version="1.9.4" date="2023-03-17">
      <description translate="no">
        <p>Minor UI improvements and bug fix.</p>
      </description>
    </release>
    <release version="1.9.3" date="2022-11-20">
      <description translate="no">
        <p>Great UI improvements and bug fixes.</p>
      </description>
    </release>
    <release version="1.9.2" date="2022-10-02">
      <description translate="no">
        <p>Support the new libadwaita widgets and greatly speed up Piped feed loading.</p>
      </description>
    </release>
    <release version="1.9.1" date="2022-08-22">
      <description translate="no">
        <p>Fix incorrect ordering of subscriptions in the subscriptions list</p>
      </description>
    </release>
    <release version="1.9.0" date="2022-08-13">
      <description translate="no">
        <p>Settings and UI for importing subscriptions</p>
      </description>
    </release>
    <release version="1.8.5" date="2022-07-31">
      <description translate="no">Dutch and Czech translations</description>
    </release>
    <release version="1.8.4" date="2022-07-16">
      <description translate="no">Fix playing local videos for some video formats</description>
    </release>
    <release version="1.8.3" date="2022-07-05">
      <description translate="no">Allow for custom Piped URLs, view local videos</description>
    </release>
    <release version="1.8.2" date="2022-06-25">
      <description translate="no">Russian translation, symbolic app icon</description>
    </release>
    <release version="1.8.1" date="2022-06-22">
      <description translate="no">Fix compilation</description>
    </release>
    <release version="1.8.0" date="2022-06-22">
      <description translate="no">New icon (thanks @daudix-UFO), better adaptive UI for bigger screens</description>
    </release>
    <release version="1.7.1" date="2022-06-11">
      <description translate="no">Fix flatpak build</description>
    </release>
    <release version="1.7.0" date="2022-06-11">
      <description translate="no">Look at the videos of a single subscription using a dedicated button in the subscriptions list</description>
    </release>
    <release version="1.6.6" date="2022-04-27">
      <description translate="no">
        <p>Minor fix with the italic translation preventing packaging</p>
      </description>
    </release>
    <release version="1.6.5" date="2022-04-27">
      <description translate="no">
        <p>Italian translation</p>
      </description>
    </release>
    <release version="1.6.4" date="2022-04-23">
      <description translate="no">
        <p>Fix crash when the systems locale was not translated yet.</p>
      </description>
    </release>
    <release version="1.6.3" date="2022-04-22">
      <description translate="no">
        <p>Fix some subscription names not showing and some icons not theming correctly.</p>
      </description>
    </release>
    <release version="1.6.2" date="2022-04-09">
      <description translate="no">
        <p>Fix for some subscriptions not loading</p>
      </description>
    </release>
    <release version="1.6.1" date="2022-03-29">
      <description translate="no">
        <p>Hotfix: Fixed a bug where only one subscription could have been added at a time.</p>
      </description>
    </release>
    <release version="1.6.0" date="2022-03-24">
      <description translate="no">
        <p>Port to GTK4 with UI-improvements and internationalization.</p>
      </description>
    </release>
    <release version="1.5.0" date="2021-12-02">
      <description translate="no">
        <p>Add support for downloading videos and copying the link to clipboard.</p>
      </description>
    </release>
    <release version="1.4.0" date="2021-10-22">
      <description translate="no">
        <p>Added support for the platforms peertube and lbry.</p>
      </description>
    </release>
    <release version="1.3.1" date="2021-09-28">
      <description translate="no">
        <p>Hotfix flatpak not compiling.</p>
      </description>
    </release>
    <release version="1.3.0" date="2021-09-28">
      <description translate="no">
        <p>Major refactoring, no big changes to features or gui but preparations for such features.</p>
      </description>
    </release>
    <release version="1.2.3" date="2021-07-08">
      <description translate="no">
        <p>Made the application visible in phoshs "show only adaptive apps".</p>
      </description>
    </release>
    <release version="1.2.2" date="2021-07-08">
      <description translate="no">
        <p>Fixed too small window size, header bar not having a close button and not beeing draggable.</p>
      </description>
    </release>
    <release version="1.2.1" date="2021-06-30">
      <description translate="no">
        <p>Stylistic upgrades and flatpak packaging format</p>
      </description>
    </release>
    <release version="1.2.0" date="2021-05-19"/>
  </releases>

  <description>
    <!-- Translators: Part of the description of the application in the metainfo. -->
    <p>
      Pipeline lets you watch and download videos from YouTube and PeerTube, all without needing to navigate through
      different websites. Its adaptive design allows you to enjoy content on any screen size.
    </p>
    <!-- Translators: Part of the description of the application in the metainfo. -->
    <p>
      Pipeline allows you to:
    </p>
    <!-- Translators: Part of the description of the application in the metainfo. -->
    <ul>
      <!-- Translators: Part of the description of the application in the metainfo. -->
      <li>Search for you favorite channels and subscribe to them.</li>
      <!-- Translators: Part of the description of the application in the metainfo. -->
      <li>Aggregate the videos of all subscriptions into a single feed.</li>
      <!-- Translators: Part of the description of the application in the metainfo. -->
      <li>Filter out unwanted items from the feed, like short videos or videos from a series.</li>
      <!-- Translators: Part of the description of the application in the metainfo. -->
      <li>Play those videos either in the built-in video player or with any other video player of your choice.</li>
      <!-- Translators: Part of the description of the application in the metainfo. -->
      <li>Download videos for offline viewing.</li>
      <!-- Translators: Part of the description of the application in the metainfo. -->
      <li>Manage videos you want to watch later.</li>
      <!-- Translators: Part of the description of the application in the metainfo. -->
      <li>Import your subscriptions from NewPipe or YouTube.</li>
      <!-- Translators: Part of the description of the application in the metainfo. -->
      <li>Supports YouTube, PeerTube with the possibility to extend to other platforms on request.</li>
    </ul>
  </description>

  <screenshots>
    <screenshot type="default">
      <caption>Playing a Video</caption>
      <image>https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/screenshots/video.png</image>
    </screenshot>
    <screenshot type="default">
      <caption>Viewing Videos of a Channel</caption>
      <image>https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/screenshots/channel.png</image>
    </screenshot>
  </screenshots>

  <url type="homepage">https://mobile.schmidhuberj.de/pipeline/</url>
  <url type="bugtracker">https://gitlab.com/schmiddi-on-mobile/pipeline/issues</url>
  <url type="help">https://gitlab.com/schmiddi-on-mobile/pipeline/-/wikis/home</url>
  <url type="contact">https://matrix.to/#/#pipelineapp:matrix.org</url>
  <url type="vcs-browser">https://gitlab.com/schmiddi-on-mobile/pipeline</url>
  <url type="contribute">https://gitlab.com/schmiddi-on-mobile/pipeline/-/blob/master/CONTRIBUTING.md</url>
  <url type="translate">https://hosted.weblate.org/engage/schmiddi-on-mobile/</url>

  <provides>
    <binary>tubefeeder</binary>
  </provides>
  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
    <internet>always</internet>
  </recommends>
  <requires>
    <display_length compare="ge" side="shortest">300</display_length>
    <display_length compare="ge" side="longest">360</display_length>
  </requires>

  <branding>
    <color type="primary" scheme_preference="light">#62a0ea</color>
    <color type="primary" scheme_preference="dark">#1a5fb4</color>
  </branding>
</component>
