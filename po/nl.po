# ITALIAN TRANSLATION FOR TUBEFEEDER.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# ALBANO BATTISTELLA <ALBANO_BATTISTELLA@HOTMAIL.COM>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.5.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-11-06 10:41+0100\n"
"PO-Revision-Date: 2024-11-02 20:00+0000\n"
"Last-Translator: Philip Goto <philip.goto@gmail.com>\n"
"Language-Team: Dutch <https://hosted.weblate.org/projects/schmiddi-on-mobile/"
"pipeline/nl/>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.8.2\n"

#: data/de.schmidhuberj.tubefeeder.desktop.in.in:5
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:6
#: data/resources/ui/window.blp:9 src/gui/video_page.rs:319
msgid "Pipeline"
msgstr "Pipeline"

#: data/de.schmidhuberj.tubefeeder.desktop.in.in:6
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:7
msgid "Follow video creators"
msgstr "Volg video­makers"

#: data/de.schmidhuberj.tubefeeder.desktop.in.in:12
msgid "youtube;peertube;video;"
msgstr "youtube;peertube;video;"

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:331
#, fuzzy
#| msgid ""
#| "Pipeline lets you watch and download videos from YouTube and PeerTube, "
#| "all without needing to navigate through different websites."
msgid ""
"Pipeline lets you watch and download videos from YouTube and PeerTube, all "
"without needing to navigate through different websites. Its adaptive design "
"allows you to enjoy content on any screen size."
msgstr ""
"Met Pipeline kunt u video's van YouTube en PeerTube bekĳken en downloaden, "
"allemaal zonder dat u door verschillende websites hoeft te navigeren."

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:336
msgid "Pipeline allows you to:"
msgstr ""

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:342
msgid "Search for you favorite channels and subscribe to them."
msgstr ""

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:344
msgid "Aggregate the videos of all subscriptions into a single feed."
msgstr ""

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:346
msgid ""
"Filter out unwanted items from the feed, like short videos or videos from a "
"series."
msgstr ""

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:348
msgid ""
"Play those videos either in the built-in video player or with any other "
"video player of your choice."
msgstr ""

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:350
msgid "Download videos for offline viewing."
msgstr ""

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:352
msgid "Manage videos you want to watch later."
msgstr ""

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:354
#, fuzzy
#| msgid "Import subscriptions from NewPipe or YouTube"
msgid "Import your subscriptions from NewPipe or YouTube."
msgstr "Importeer abonnementen van NewPipe of YouTube"

#. Translators: Part of the description of the application in the metainfo.
#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:356
msgid ""
"Supports YouTube, PeerTube with the possibility to extend to other platforms "
"on request."
msgstr ""

#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:362
msgid "Playing a Video"
msgstr ""

#: data/de.schmidhuberj.tubefeeder.metainfo.xml.in:366
msgid "Viewing Videos of a Channel"
msgstr ""

#: data/de.schmidhuberj.tubefeeder.gschema.xml:6
msgid "Window width"
msgstr "Venster­breedte"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:10
msgid "Window height"
msgstr "Venster­hoogte"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:14
msgid "Window maximized state"
msgstr "Maximalisatie­staat van venster"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:18
msgid "The player to use"
msgstr "Te gebruiken speler"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:22
msgid "The downloader to use"
msgstr "Te gebruiken downloader"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:26
msgid "The piped api url"
msgstr "Piped-API-URL"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:30
#, fuzzy
#| msgid "The piped api url"
msgid "The piped api urls"
msgstr "Piped-API-URL"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:34
msgid "The peertube api used for searches"
msgstr ""

#: data/de.schmidhuberj.tubefeeder.gschema.xml:38
msgid "Only show videos of yesterday."
msgstr "Alleen video's van gisteren tonen"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:42
msgid "Remove short videos."
msgstr "Korte video's verwĳderen"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:46
msgid "Whether the filter is enabled."
msgstr ""

#: data/de.schmidhuberj.tubefeeder.gschema.xml:50
msgid "The type of platform for which the last subscription was added."
msgstr "Het type platform waarvoor het laatste abonnement is toegevoegd"

#: data/de.schmidhuberj.tubefeeder.gschema.xml:54
msgid "Whether to reload when starting the application."
msgstr ""

#: data/de.schmidhuberj.tubefeeder.gschema.xml:58
msgid "Whether to play with the external player by default."
msgstr ""

#: data/de.schmidhuberj.tubefeeder.gschema.xml:64
msgid "Stores last volume value to apply on startup"
msgstr ""

#: data/de.schmidhuberj.tubefeeder.gschema.xml:68
msgid "Stores last speed value to apply on startup"
msgstr ""

#: data/resources/ui/channel_page.blp:63
#, fuzzy
#| msgid "Subscriptions"
msgid "Description"
msgstr "Abonnementen"

#: data/resources/ui/error_dialog.blp:12
msgid "Errors"
msgstr "Fouten"

#: data/resources/ui/error_dialog.blp:20
msgid "Clear Errors"
msgstr "Fouten wissen"

#: data/resources/ui/filter_dialog.blp:6
#: data/resources/ui/preferences_dialog.blp:6
#: data/resources/ui/preferences_dialog.blp:10
#: data/resources/ui/shortcuts.blp:47
msgid "General"
msgstr "Algemeen"

#: data/resources/ui/filter_dialog.blp:13
msgid ""
"Filters are currently disabled. Those settings will not have an effect until "
"you enable filters again."
msgstr ""

#: data/resources/ui/filter_dialog.blp:18
msgid "General Filters"
msgstr "Algemene filters"

#: data/resources/ui/filter_dialog.blp:21
msgid "Hide Short Videos"
msgstr "Korte video's verbergen"

#: data/resources/ui/filter_dialog.blp:22
#, fuzzy
#| msgid "Removes all videos shorter than 1 minute"
msgid "Hide videos shorter than one minute."
msgstr "Verwijdert alle video's korter dan 1 minuut"

#: data/resources/ui/filter_dialog.blp:26
#, fuzzy
#| msgid "Show only Videos from Yesterday"
msgid "Hide Videos from Today"
msgstr "Alleen video's van gisteren tonen"

#: data/resources/ui/filter_dialog.blp:27
msgid "Hide videos which came out today"
msgstr ""

#: data/resources/ui/filter_dialog.blp:32
msgid "Custom Filters"
msgstr "Aangepaste filters"

#: data/resources/ui/filter_dialog.blp:33
msgid ""
"Filter out videos that contain a given title and were uploaded by a given "
"channel name"
msgstr ""

#: data/resources/ui/filter_dialog.blp:51
msgid "Video Title"
msgstr "Video­titel"

#: data/resources/ui/filter_dialog.blp:55
msgid "Channel Name"
msgstr "Kanaalnaam"

#: data/resources/ui/filter_dialog.blp:59
msgid "Add Filter"
msgstr "Filter toevoegen"

#: data/resources/ui/preferences_dialog.blp:13
msgid "Reload on Startup"
msgstr ""

#: data/resources/ui/preferences_dialog.blp:18
msgid "External Programs"
msgstr "Externe apps"

#: data/resources/ui/preferences_dialog.blp:21
msgid "Play with External Player by Default"
msgstr ""

#: data/resources/ui/preferences_dialog.blp:24
msgid "External Video Player"
msgstr ""

#: data/resources/ui/preferences_dialog.blp:30
msgid "External Video Downloader"
msgstr ""

#: data/resources/ui/preferences_dialog.blp:38
msgid "Piped APIs"
msgstr "Piped-API's"

#: data/resources/ui/preferences_dialog.blp:39
msgid ""
"The Piped instances Pipeline will use to fetch YouTube videos from. The "
"first instance will be primarily used, while the other instances are "
"fallbacks in case the previous instances all failed. For a complete list of "
"public APIs, see the <a href=\"https://github.com/TeamPiped/Piped/wiki/"
"Instances\">Instances page</a>."
msgstr ""

#: data/resources/ui/preferences_dialog.blp:51
msgid "Peertube Preferences"
msgstr "PeerTube-voorkeuren"

#: data/resources/ui/preferences_dialog.blp:54
msgid "Search Instance"
msgstr "Instantie doorzoeken"

#: data/resources/ui/shortcuts.blp:8
msgid "Application"
msgstr "App"

#: data/resources/ui/shortcuts.blp:12
#, fuzzy
#| msgid "Subscribe to channels"
msgid "Bookmark Video or Subscribe to Channel"
msgstr "Abonneer op kanalen"

#: data/resources/ui/shortcuts.blp:17
msgid "Reload Video Feed or Channel Page"
msgstr ""

#: data/resources/ui/shortcuts.blp:22
msgid "Play or Pause the Video"
msgstr ""

#: data/resources/ui/shortcuts.blp:23
msgid "Only when the video page is in focus"
msgstr ""

#: data/resources/ui/shortcuts.blp:28
msgid "Pages"
msgstr "Pagina's"

#: data/resources/ui/shortcuts.blp:32
msgid "Switch to Feed Page"
msgstr ""

#: data/resources/ui/shortcuts.blp:37
msgid "Switch to Watch-Later Page"
msgstr ""

#: data/resources/ui/shortcuts.blp:42
#, fuzzy
#| msgid "No Subscriptions"
msgid "Switch to Subscriptions Page"
msgstr "U heeft geen abonnementen"

#: data/resources/ui/shortcuts.blp:52 data/resources/ui/window.blp:149
#: data/resources/ui/window.blp:305
msgid "Search"
msgstr "Zoeken"

#: data/resources/ui/shortcuts.blp:57 data/resources/ui/window.blp:40
msgid "Go Back"
msgstr "Terug"

#: data/resources/ui/shortcuts.blp:62
msgid "Quit"
msgstr "Afsluiten"

#: data/resources/ui/shortcuts.blp:67
msgid "Show Shortcuts"
msgstr "Snel­toetsen tonen"

#: data/resources/ui/shortcuts.blp:72
msgid "Show Preferences"
msgstr "Voorkeuren tonen"

#: data/resources/ui/shortcuts.blp:77
msgid "Open Menu"
msgstr "Menu openen"

#: data/resources/ui/video_page.blp:49
msgid "Video is Playing Externally"
msgstr ""

#: data/resources/ui/video_page.blp:175 src/gui/video_item.rs:161
msgid "Download"
msgstr "Downloaden"

#: data/resources/ui/video_page.blp:193
#, fuzzy
#| msgid "No Videos Found"
msgid "Copy Video Link"
msgstr "Er zijn geen video's gevonden"

#. I don't think this will ever be shown.
#: data/resources/ui/window.blp:31
msgid "Main Page"
msgstr "Hoofd­pagina"

#: data/resources/ui/window.blp:57 data/resources/ui/window.blp:134
msgid "Reload"
msgstr "Herladen"

#: data/resources/ui/window.blp:91
msgid "Video"
msgstr "Video"

#: data/resources/ui/window.blp:103
msgid "Channel"
msgstr "Kanaal"

#. I don't think this will ever be shown.
#: data/resources/ui/window.blp:118
msgid "Sidebar"
msgstr "Zĳbalk"

#: data/resources/ui/window.blp:156
msgctxt "accessibility"
msgid "Primary menu"
msgstr "Hoofdmenu"

#: data/resources/ui/window.blp:160
msgctxt "tooltip"
msgid "Primary menu"
msgstr "Hoofdmenu"

#: data/resources/ui/window.blp:186
msgid "Search for Videos and Channels"
msgstr ""

#: data/resources/ui/window.blp:203
msgid "Feed"
msgstr "Feed"

#: data/resources/ui/window.blp:230 data/resources/ui/window.blp:288
#, fuzzy
#| msgid "No Subscriptions"
msgid "No Subscriptions Yet"
msgstr "U heeft geen abonnementen"

#: data/resources/ui/window.blp:231 data/resources/ui/window.blp:289
#, fuzzy
#| msgid "Subscribe to channels"
msgid "Subscribe to channels first"
msgstr "Abonneer op kanalen"

#: data/resources/ui/window.blp:238
msgid "Loading Videos"
msgstr "Video's laden"

#: data/resources/ui/window.blp:245 data/resources/ui/window.blp:272
msgid "No Videos"
msgstr "Geen video's"

#: data/resources/ui/window.blp:246
#, fuzzy
#| msgctxt "accessibility"
#| msgid "Reload"
msgid "Try Reloading"
msgstr "Herladen"

#: data/resources/ui/window.blp:255 src/gui/video_item.rs:138
msgid "Watch Later"
msgstr "Later bekijken"

#: data/resources/ui/window.blp:273
msgid "Add videos to your watch later list to display them here"
msgstr ""

#: data/resources/ui/window.blp:281
msgid "Subscriptions"
msgstr "Abonnementen"

#: data/resources/ui/window.blp:333
msgid "Searching"
msgstr "Aan het zoeken"

#: data/resources/ui/window.blp:344
msgid "Show"
msgstr "Tonen"

#: data/resources/ui/window.blp:351
msgid "Manage Filters"
msgstr "Filters beheren"

#: data/resources/ui/window.blp:357
msgid "Toggle Filters"
msgstr "Filters omschakelen"

#: data/resources/ui/window.blp:365
msgid "Import"
msgstr "Importeren"

#: data/resources/ui/window.blp:369
msgid "Preferences"
msgstr "Voorkeuren"

#: data/resources/ui/window.blp:374
msgid "Keyboard Shortcuts"
msgstr "Snel­toetsen"

#: data/resources/ui/window.blp:378
msgid "About Pipeline"
msgstr "Over Pipeline"

#: src/backend/channel.rs:87
msgid "Unknown Channel"
msgstr "Onbekend kanaal"

#: src/backend/search_stream.rs:96
#, fuzzy
#| msgid "Error parsing one subscription"
#| msgid_plural "Error parsing {} subscriptions"
msgid "Error searching one platform"
msgstr "Eén abonnement kan niet worden verwerkt"

#: src/backend/video_fetcher_stream.rs:130
#, fuzzy
#| msgid "Error parsing one subscription"
#| msgid_plural "Error parsing {} subscriptions"
msgid "Error loading one channel"
msgid_plural "Error loading {} channels"
msgstr[0] "Eén abonnement kan niet worden verwerkt"
msgstr[1] "{} abonnementen kunnen niet worden verwerkt"

#: src/gui/channel_item.rs:86 src/gui/window.rs:231
msgid "Unsubscribe"
msgstr "Deabonneren"

#: src/gui/channel_item.rs:91 src/gui/window.rs:233
msgid "Subscribe"
msgstr "Abonneren"

#: src/gui/channel_page.rs:34
#, fuzzy
#| msgid "Failed to Download Video"
msgid "Failed to load channel data"
msgstr "Video downloaden mislukt"

#: src/gui/channel_page.rs:249 src/gui/video_page.rs:228
msgid "one subscriber"
msgid_plural "{} subscribers"
msgstr[0] ""
msgstr[1] ""

#: src/gui/preferences_dialog.rs:254
#, fuzzy
#| msgid "Piped API"
msgid "New Piped API"
msgstr "Piped-api"

#. Note to translators: Format numbers compactly. For numbers at least one billion.
#: src/gui/utility.rs:61
msgctxt "number-formatting"
msgid "{}B"
msgstr "{} mld."

#. Note to translators: Format numbers compactly. For numbers at least one million.
#: src/gui/utility.rs:65
msgctxt "number-formatting"
msgid "{}M"
msgstr "{} mln."

#. Note to translators: Format numbers compactly. For numbers at least one thousand.
#: src/gui/utility.rs:69
msgctxt "number-formatting"
msgid "{}K"
msgstr "dznd."

#: src/gui/utility.rs:87
#, fuzzy
#| msgid "Failed to Download Video"
msgid "{title} uploaded by {channel} on {date}"
msgstr "Video downloaden mislukt"

#. Translators: formatting of time of day in a human-readable fashion, see https://docs.rs/chrono/latest/chrono/format/strftime/index.html#specifiers
#: src/gui/utility.rs:100
msgid "%H:%M"
msgstr "%H:%M"

#: src/gui/utility.rs:102
msgid "Tomorrow"
msgstr "Morgen"

#: src/gui/utility.rs:104
msgid "Yesterday"
msgstr "Gisteren"

#. Translators: formatting of dates without year in a human-readable fashion, see https://docs.rs/chrono/latest/chrono/format/strftime/index.html#specifiers
#: src/gui/utility.rs:107
msgid "%a, %d. %B"
msgstr ""

#. Translators: formatting of dates with year in a human-readable fashion, see https://docs.rs/chrono/latest/chrono/format/strftime/index.html#specifiers
#: src/gui/utility.rs:110
msgid "%Y-%m-%d"
msgstr ""

#: src/gui/video_item.rs:133
#, fuzzy
#| msgid "Watch Later"
msgid "Remove from Watch Later"
msgstr "Later bekijken"

#: src/gui/video_item.rs:145
#, fuzzy
#| msgid "Play videos with any video player"
msgid "Play with Internal Player"
msgstr "Speel video's af met elke video­speler"

#: src/gui/video_item.rs:150
msgid "Play with External Player"
msgstr ""

#: src/gui/video_item.rs:156
#, fuzzy
#| msgid "No Videos Found"
msgid "Copy Video URL"
msgstr "Er zijn geen video's gevonden"

#: src/gui/video_page.rs:70
#, fuzzy
#| msgid "Play videos with any video player"
msgid "Failed to spawn video player"
msgstr "Speel video's af met elke video­speler"

#: src/gui/video_page.rs:193
#, fuzzy
#| msgid "Failed to Play Video"
msgid "Failed to populate video information"
msgstr "Video afspelen mislukt"

#: src/gui/video_page.rs:204
#, fuzzy
#| msgid "Failed to add subscription"
msgid "Failed to fetch channel information"
msgstr "Abonnement toevoegen mislukt"

#: src/gui/video_page.rs:237
msgid "Uploaded {}"
msgstr "{} geüpload"

#: src/gui/video_page.rs:282
#, fuzzy
#| msgid "Watch Later"
msgid "Remove video from Watch Later"
msgstr "Later bekijken"

#: src/gui/video_page.rs:284
#, fuzzy
#| msgid "Watch Later"
msgid "Add video to Watch Later"
msgstr "Later bekijken"

#: src/gui/window.rs:78
msgid "One Error"
msgid_plural "{} Errors"
msgstr[0] ""
msgstr[1] ""

#: src/gui/window.rs:394
msgid "Copied URL"
msgstr "URL gekopieerd"

#: src/gui/window.rs:420
msgid "Started Download"
msgstr "Download gestart"

#: src/gui/window.rs:427
#, fuzzy
#| msgid "Play videos with any video player"
msgid "Failed to spawn video downloader"
msgstr "Speel video's af met elke video­speler"

#: src/gui/window.rs:432
msgid "Finished Download"
msgstr "Download voltooid"

#: src/gui/window.rs:680
#, fuzzy
#| msgid "Failed to add subscription"
msgid "Failed to set up application"
msgstr "Abonnement toevoegen mislukt"

#, fuzzy
#~| msgid "Copy to Clipboard"
#~ msgid "Copy URL to Clipboard"
#~ msgstr "Kopiëren naar klembord"

#~ msgid "Download Video"
#~ msgstr "Video downloaden"

#~ msgid "Pipeline comes with several features:"
#~ msgstr "Pipeline wordt geleverd met verschillende functies:"

#~ msgid "Subscribe to channels"
#~ msgstr "Abonneer op kanalen"

#~ msgid "Filter out unwanted videos in the feed"
#~ msgstr "Filter ongewenste video's uit de feed"

#~ msgid "Multiple platforms"
#~ msgstr "Meerdere platforms"

#~ msgid "Feed List"
#~ msgstr "Feed­lĳst"

#~ msgid "Filter List"
#~ msgstr "Filter­lĳst"

#~ msgid "translator-credits"
#~ msgstr ""
#~ "Heimen Stoffels <vistausss@fastmail.com>\n"
#~ "Philip Goto https://philipgoto.nl/"

#~ msgid "Open in Browser"
#~ msgstr "Openen in browser"

#~ msgid "More Information"
#~ msgstr "Meer informatie"

#~ msgid "Please check your downloader in the preferences."
#~ msgstr "Controleer uw downloader onder instellingen."

#~ msgid "Close"
#~ msgstr "Sluiten"

#~ msgid "Please check your player in the preferences."
#~ msgstr "Controleer uw speler onder instellingen."

#~ msgctxt "tooltip"
#~ msgid "Reload"
#~ msgstr "Herladen"

#~ msgid "Your feed will appear once you've added subscriptions"
#~ msgstr "Uw feed verschijnt als u abonnementen heeft toegevoegd"

#~ msgid "Manage Subscriptions…"
#~ msgstr "Abonnementen beheren…"

#~ msgid "Remove"
#~ msgstr "Verwĳderen"

#~ msgid "Filters"
#~ msgstr "Filters"

#~ msgctxt "accessibility"
#~ msgid "Add Filter…"
#~ msgstr "Filter toevoegen…"

#~ msgctxt "tooltip"
#~ msgid "Add Filter…"
#~ msgstr "Filter toevoegen…"

#~ msgid "Add Filter…"
#~ msgstr "Filter toevoegen…"

#~ msgid "Cancel"
#~ msgstr "Annuleren"

#~ msgid "Add"
#~ msgstr "Toevoegen"

#~ msgid "Import Subscriptions"
#~ msgstr "Abonnementen importeren"

#~ msgid ""
#~ "This will import your subscriptions from exported data from NewPipe or "
#~ "YouTube."
#~ msgstr ""
#~ "Hiermee worden al uw abonnementen geïmporteerd uit de geëxporteerde "
#~ "gegevens van NewPipe of YouTube."

#~ msgid "NewPipe"
#~ msgstr "NewPipe"

#~ msgid "YouTube"
#~ msgstr "YouTube"

#~ msgid "Player"
#~ msgstr "Speler"

#~ msgid "APIs"
#~ msgstr "Api's"

#~ msgid ""
#~ "For a complete list of public APIs, see the <a href=\"https://github.com/"
#~ "TeamPiped/Piped/wiki/Instances\">Instances page</a>."
#~ msgstr ""
#~ "De lijst met openbare api's is te vinden op de <a href=\"https://github."
#~ "com/TeamPiped/Piped/wiki/Instances\">instantiespagina</a> (Engels)."

#~ msgid "Custom Piped API"
#~ msgstr "Aangepaste Piped-api"

#~ msgid "Other"
#~ msgstr "Overig"

#~ msgid "Subscribe to a Channel…"
#~ msgstr "Abonneren op kanaal…"

#~ msgid "Add Subscription"
#~ msgstr "Abonnement toevoegen"

#~ msgid "Base URL"
#~ msgstr "Hoofdurl"

#~ msgid "Channel ID or Name"
#~ msgstr "Kanaalid of -naam"

#~ msgctxt "accessibility"
#~ msgid "Add Subscription…"
#~ msgstr "Abonnement toevoegen…"

#~ msgctxt "tooltip"
#~ msgid "Add Subscription…"
#~ msgstr "Abonnement toevoegen…"

#~ msgctxt "accessibility"
#~ msgid "Go back to subscription list"
#~ msgstr "Terug naar abonnementenlijst"

#~ msgctxt "tooltip"
#~ msgid "Go back"
#~ msgstr "Ga terug"

#~ msgid "Please check your input, internet connection and API URL."
#~ msgstr "Controleer uw invoer, internet­verbinding en API-URL."

#~ msgid "Video Information"
#~ msgstr "Video­informatie"

#~ msgid "Everything Watched"
#~ msgstr "Kijkgeschiedenis"

#~ msgid "How about going outside?"
#~ msgstr "Waarom gaat u niet even lekker naar buiten?"

#~ msgid "Error connecting to the network"
#~ msgstr "Er kan geen verbinding worden gemaakt met het internet"

#~ msgid "Some error occured"
#~ msgstr "Er is een fout opgetreden"

#~ msgid "%F %T"
#~ msgstr "%F %T"

#~ msgid "Select NewPipe subscriptions file"
#~ msgstr "Kies het NewPipe-abonnementsbestand"

#~ msgid "Failure to import subscriptions"
#~ msgstr "De abonnementen kunnen niet worden geïmporteerd"

#~ msgid "Select YouTube subscription file"
#~ msgstr "Kies het YouTube-abonnementsbestand"

#~ msgid ""
#~ "Note that on Flatpak, there are some more steps required when using a "
#~ "player external to the Flatpak. For more information, please consult the "
#~ "wiki."
#~ msgstr ""
#~ "Let op: de flatpakversie vereist meer stappen bij gebruik van een externe "
#~ "speler. Bekijk voor meer informatie de wiki."

#~ msgid "Donate"
#~ msgstr "Doneren"

#~ msgid "Load more"
#~ msgstr "Meer laden"

#~ msgid "Settings"
#~ msgstr "Instellingen"

#~ msgid "About"
#~ msgstr "Over"

#~ msgid "Overwritten by environmental variable."
#~ msgstr "Overschreven door een omgevingsvariabele."
